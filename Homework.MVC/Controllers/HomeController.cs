﻿using Homework.MVC.Models;
using Microsoft.AspNetCore.Mvc;

namespace Homework.MVC.Controllers
{
    public class HomeController : Controller
    {
        IUserRepository repository;

        public HomeController(IUserRepository repository)
        {
            this.repository = repository;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(User user)
        {
            if (!ModelState.IsValid) return View();
            
            repository.AddUser(user);
            return View("Thanks", repository.Users);

        }
    }
}