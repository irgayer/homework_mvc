﻿using Microsoft.EntityFrameworkCore;

namespace Homework.MVC.Models
{
    public class AppContext : DbContext
    {
        public AppContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }   
        
        
        public DbSet<User> Users { get; set; }
    }
}