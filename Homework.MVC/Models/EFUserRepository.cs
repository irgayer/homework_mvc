﻿using System.Collections.Generic;
using System.Linq;

namespace Homework.MVC.Models
{
    public class EFUserRepository : IUserRepository
    {
        private AppContext context;

        public EFUserRepository(AppContext context)
        {
            this.context = context;
        }
        
        public List<User> Users => context.Users.ToList();

        public void AddUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }
    }
}