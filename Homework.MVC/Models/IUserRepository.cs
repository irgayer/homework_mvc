﻿using System.Collections.Generic;

namespace Homework.MVC.Models
{
    public interface IUserRepository
    {
        public List<User> Users { get; }
        public void AddUser(User user);
    }
}