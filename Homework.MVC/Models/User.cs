﻿namespace Homework.MVC.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Secondname { get; set; }
        public bool IsMale { get; set; }
    }
}