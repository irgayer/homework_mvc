using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Homework.MVC.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AppContext = Homework.MVC.Models.AppContext;

namespace Homework.MVC
{
    public class Startup
    {
       IConfiguration Configuration { get; }

       public Startup(IConfiguration configuration)
       {
           Configuration = configuration;
       }
        
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(s => s.EnableEndpointRouting = false);
            services.AddDbContext<AppContext>(options =>
                {
                    options.UseSqlServer(Configuration.GetConnectionString("UsersConnection"));
                });

            services.AddSingleton<IUserRepository, EFUserRepository>();
        }

        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseDeveloperExceptionPage();

            app.UseMvc(routes =>
            {
                routes.MapRoute
                (
                    name: "default",
                    template: "{controller=Home}/{action=Index}"
                );
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Hello World!"); });
            });
        }
    }
}